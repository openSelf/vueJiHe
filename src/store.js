import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);


const state = {
    isShow: true,
};

const actions = {
    showPush: ({ commit }) => {
        commit("showPush");
    },
    showPull: ({ commit }) => {
        commit("showPull");
    }
};


const mutations = {
    showPush(state) {
        state.isShow = true
    },
    showPull(state) {
        state.isShow = false
    },
    isLogin(state, user) {
        state.user = user;
    }
};

const getters = {
    isShow(state) {
        return state.isShow
    },
};

export default new Vuex.Store({
    state,
    actions,
    mutations,
    getters
});