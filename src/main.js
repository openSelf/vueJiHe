import Vue from 'vue';
import App from './App.vue';

import vueRouter from 'vue-router';
Vue.use(vueRouter);
import routerConfig from './routerConfig.js';
const router = new vueRouter(routerConfig);

// 引入全局jquery
require("expose-loader?$!jquery");

//引入全局elemeng-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-default/index.css';
Vue.use(ElementUI);


//引入echarts
import echarts from 'echarts';


// 引入store
import store from './store.js';


// 全局导航钩子
router.beforeEach((to, from, next) => {
    if (to.meta.requireAuth && !window.localStorage.getItem('username')) {
        window.localStorage.setItem('backUrl', to.fullPath);
        next({
            path: '/login',
            query: {
                // 将跳转的路由path作为参数，登录成功后跳转到该路由
                redirect: to.fullPath
            }
        });

    } else {
        // window.localStorage.setItem('backUrl', to.fullPath);
        next();
    }
});

//判断object是否为空
// function isEmptyObject(obj) {
//     for (var key in obj) {
//         return false;
//     }
//     return true;
// }



new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
});