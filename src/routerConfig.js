import Login from './components/Login.vue';
import Register from './components/Register.vue';
import Error from './components/Error.vue';
import Index from './components/Index.vue';
import Qiu from './components/Qiu.vue';
import Data1 from './components/Data1.vue';
import Data2 from './components/Data2.vue';
import Data3 from './components/Data3.vue';
import Data4 from './components/Data4.vue';
import Powmanage from './components/Powmanage.vue';
import Toolmanage from './components/Toolmanage.vue';
import Localmanage1 from './components/Localmanage1.vue';
import Localmanage2 from './components/Localmanage2.vue';
import Sysmanage1 from './components/Sysmanage1.vue';
import Sysmanage2 from './components/Sysmanage2.vue';
import Sysmanage3 from './components/Sysmanage3.vue';
import Sysmanage4 from './components/Sysmanage4.vue';
import Sysmanage5 from './components/Sysmanage5.vue';
import Sysmanage6 from './components/Sysmanage6.vue';
import Sysmanage7 from './components/Sysmanage7.vue';
import Sysmanage8 from './components/Sysmanage8.vue';
import Sysmanage9 from './components/Sysmanage9.vue';
import Sysmanage10 from './components/Sysmanage10.vue';




export default {
    routes: [
        { path: '/login', component: Login },
        { path: '/register', component: Register },
        {
            path: '/index',
            meta: { requireAuth: true },
            component: Index,
            children: [
                { path: '/index/map', component: Qiu, meta: { requireAuth: true } },
                { path: '/index/data1', component: Data1, meta: { requireAuth: true } },
                { path: '/index/data2', component: Data2, meta: { requireAuth: true } },
                { path: '/index/data3', component: Data3, meta: { requireAuth: true } },
                { path: '/index/data4', component: Data4, meta: { requireAuth: true } },
                { path: '/index/powmanage', component: Powmanage, meta: { requireAuth: true } },
                { path: '/index/toolmanage', component: Toolmanage, meta: { requireAuth: true } },
                { path: '/index/localmanage1', component: Localmanage1, meta: { requireAuth: true } },
                { path: '/index/localmanage2', component: Localmanage2, meta: { requireAuth: true } },
                { path: '/index/sysmanage1', component: Sysmanage1, meta: { requireAuth: true } },
                { path: '/index/sysmanage2', component: Sysmanage2, meta: { requireAuth: true } },
                { path: '/index/sysmanage3', component: Sysmanage3, meta: { requireAuth: true } },
                { path: '/index/sysmanage4', component: Sysmanage4, meta: { requireAuth: true } },
                { path: '/index/sysmanage5', component: Sysmanage5, meta: { requireAuth: true } },
                { path: '/index/sysmanage6', component: Sysmanage6, meta: { requireAuth: true } },
                { path: '/index/sysmanage7', component: Sysmanage7, meta: { requireAuth: true } },
                { path: '/index/sysmanage8', component: Sysmanage8, meta: { requireAuth: true } },
                { path: '/index/sysmanage9', component: Sysmanage9, meta: { requireAuth: true } },
                { path: '/index/sysmanage10', component: Sysmanage10, meta: { requireAuth: true } },
                { path: '/index/error', component: Error, meta: { requireAuth: true } },
            ]
        },
        { path: '*', redirect: '/login' }
    ]
};